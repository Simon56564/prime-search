#include <stdio.h>
#include <stdlib.h>

void setup() {
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly
  String result;
  result = primeTest(20000, 30000);
  Serial.println(result);
}


// using 64 bit integers
// max means inlusive the number max
// min means inclusive the number min
String primeTest(long long min_num, long long max_num)
{
  long long i, j, start_num, index_return = 0;

  bool prime[(max_num - min_num)+1];
  memset(prime, true, sizeof(prime));

  start_num = min_num;

  for (i = start_num; i <= max_num; i++)
  {
    for (j = 2; j <= i / 2; j++)
    {
      if ((i % j) == 0)
      {
        prime[index_return] = false;
        break;
      }
    }
    index_return++;
  }
  
  String ret_string = "";
  
  for (i = 0; i < (max_num - min_num) +1; i++){
    if (prime[i]){
      // add long long compatibility
      String buff = String((int)(min_num + i));
      ret_string += buff + ", ";
    }
  }

  return ret_string;
}
