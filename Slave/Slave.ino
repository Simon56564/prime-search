
#include <SPI.h>

char rbuf [100];
volatile byte rpos;
volatile boolean process_it;

// this is near the maximum buffer size because of RAM limitations
// DONT EXCEED THIS
// NOTE: if this is too big the Serial.Print() will not work for large strings anymore
char sbuf [300];
volatile int spos;
volatile bool active;

volatile bool recieve;

void setup (void)
{
  Serial.begin (115200);   // debugging

  // turn on SPI in slave mode
  SPCR |= bit (SPE);

  // have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);

  // get ready for an interrupt
  rpos = 0;   // buffer empty
  process_it = false;

  // now turn on interrupts
  SPI.attachInterrupt();

  recieve = true;

  // to interrupt the master
  pinMode(9, OUTPUT);
  // initial interrupt
  digitalWrite(9, HIGH);
  digitalWrite(9, LOW);

  Serial.println("test");
}


// SPI interrupt routine
ISR (SPI_STC_vect) {
  byte c = SPDR;  // grab byte from SPI Data Register

  if (recieve) {
    // add to buffer if room
    if (rpos < (sizeof (rbuf) - 1))
      rbuf [rpos++] = c;

    // example: newline means time to process buffer
    if (c == '\n')
      process_it = true;
  }
  else {
    if (c == 1)  // starting new sequence?
    {
      active = true;
      spos = 0;
      SPDR = sbuf [spos++];   // send first byte
      return;
    }

    if (!active)
    {
      SPDR = 0;
      return;
    }

    SPDR = sbuf [spos];
    if (sbuf [spos] == 0 || ++spos >= sizeof (sbuf))
      active = false;
  }

}  // end of interrupt routine SPI_STC_vect

// main loop - wait for flag set in interrupt routine
void loop (void)
{
  // Serial.println("Main Loop");
  if (process_it)
  {
    rbuf [rpos] = 0;

    // Split recieved String and execute primeSearch
    // Here is computation work done
    Serial.println(rbuf);
    String buffer_string = String(rbuf);
    int split = buffer_string.indexOf(',');
    String min_num_string = buffer_string.substring(0, split);
    String max_num_string = buffer_string.substring(split + 2, buffer_string.length());
    // toInt() returns a long
    String result = primeTest(min_num_string.toInt(), max_num_string.toInt());
    Serial.println("Result: " + result);

    // put the result in the sent buffer
    int i;
    for(i = 0; i < result.length(); i++){
      sbuf[i] = result[i];
    }
    sbuf[result.length()] = 0;
    Serial.println(sbuf);

    // now send the result
    recieve = false;
    
    // say that we are finised here
    digitalWrite(9, HIGH);
    delay(10);
    digitalWrite(9, LOW);

    rpos = 0;

    // we are finised sending our thing
    recieve = true;
    process_it = false;
  }  // end of flag set

}  // end of loop

// Serial.println(sizeof(3)); => 2
// Serial.println(sizeof(3L)); => 4
// we use long on both arduinos
// Keep code always the same as in Master
String primeTest(long min_num, long max_num) {
  long i, j, start_num, index_return = 0L;

  bool prime[(max_num - min_num) + 1];
  memset(prime, true, sizeof(prime));

  start_num = min_num;

  // here the computational work is done
  for (i = start_num; i <= max_num; i++) {
    for (j = 2; j <= i / 2; j++) {
      if ((i % j) == 0) {
        prime[index_return] = false;
        break;
      }
    }
    index_return++;
  }

  String ret_string = "";

  for (i = 0; i < (max_num - min_num) + 1; i++) {
    if (prime[i]) {
      // add long long compatibility
      String buff = String(min_num + i);
      ret_string += buff + ", ";
    }
  }

  if(ret_string.equals("")){
    return "NULL";
  }
  
  return ret_string.substring(0, ret_string.length() - 2);
}
