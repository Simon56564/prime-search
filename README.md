# Prime-Search

Nicest parallelized Prime number search Project

# Protocoll

String sent to slave should look the following: "<number1>, <number2>"

# SPI

We now use SPI. When slave has to talk, he interrupts the master which then initiates the transmisson from the slave to the master.

# i2c

We don't use i2c because https://github.com/arduino/ArduinoCore-arc32/issues/112

# Arduino

## Uno
Can be master or slave for SPI


(https://store.arduino.cc/usa/arduino-uno-rev3)

CPU 8 Bit: ATmega328P-PU

## 101

Cannot be SPI Slave (Slave:47: error: expected constructor, destructor, or type conversion before '(' token

 ISR (SPI_STC_vect) {
)

So 101 has to be SPI master.
It would be more impressive if faster microcontroller (101) would be slave

(https://store.arduino.cc/genuino-101)

CPU 32 Bit: Intel� Quark� SE C1000 Microcontroller
