#include <SPI.h>

String slave_answer = "";

// should work until 31397 where a prime gap of 72 will occur => return NULL value
long SLOT_SIZE = 50;
long calculated_min = 1;
long calculated_max = SLOT_SIZE;

// to reassign slave that slot
long current_slave_min = -1;
long current_slave_max = -1;

unsigned long last_master_calculation = 0;
unsigned long max_master_calculation = 0;

unsigned long slaveStartTime = 0;


// if slave takes 50 times longer than the last master calculation it will be restartet.
int SLAVE_RESTART_FACTOR = 50;

volatile boolean send_slave_data;
volatile boolean recieved_slave_data;


void setup (void) {
  Serial.begin (115200);   // debugging

  digitalWrite(SS, HIGH);  // ensure SS stays high for now

  // Put SCK, MOSI, SS pins into output mode
  // also put SCK, MOSI into LOW state, and SS into HIGH state.
  // Then put SPI hardware into Master mode and turn SPI on
  SPI.begin ();

  // Slow down the master a bit
  SPI.setClockDivider(SPI_CLOCK_DIV8);

  // try enabeling interrupts from slave
  pinMode(9, INPUT);
  attachInterrupt(digitalPinToInterrupt(9), interrupt, RISING);

  // send first problem to slave. The next sending is only done in the interrupt from slave.
  // Slave asks for more work,
  send_slave_data = false;
  recieved_slave_data = false;

  // delay only for debugging reasons now
  delayMicroseconds (10000000);

  // send first task to slave
  send_to_slave(calculated_min, calculated_max);
    
  // update the search space
  calculated_min += SLOT_SIZE;
  calculated_max += SLOT_SIZE;
}  // end of setup


void loop (void) {
  // ensure that the Slave interrupt does not mess around with our boundaries
  noInterrupts();
  calculated_min += SLOT_SIZE;
  calculated_max += SLOT_SIZE;
  interrupts();

  // this can be interrupted but the new slots are already allocated
  // TODO: redo this with 2 different variables
  unsigned long startTime = millis();
  String master_result = primeTest(calculated_min - SLOT_SIZE, calculated_max - SLOT_SIZE);
  unsigned long endTime = millis();
  last_master_calculation = endTime - startTime;

  // update max_master_calculation
  if(last_master_calculation > max_master_calculation){
    max_master_calculation = last_master_calculation;
  }
  
  // Serial.println("Master time: " + String(last_master_calculation / 1000.0));
  Serial.println("Master return: " + master_result);

  if(recieved_slave_data){
    Serial.println("Slave return: " + slave_answer);
    recieved_slave_data = false;
  }

  // if we didnt hear anything quite long from the slave
  if(millis() - slaveStartTime > SLAVE_RESTART_FACTOR * max_master_calculation){
    Serial.println("!!!Resetted Slave after " + String((millis() - slaveStartTime) / 1000.0) + " s!!!");

    // resend slave his task
    send_to_slave(current_slave_min, current_slave_max);
    send_slave_data = false;
  }

  // if slave interrupted master
  if(send_slave_data){
    send_to_slave(calculated_min, calculated_max);
    
    // update the search space
    calculated_min += SLOT_SIZE;
    calculated_max += SLOT_SIZE;
    send_slave_data = false;
  }

}

void interrupt() {
  // interrupt is called when slave has finised calculating something
  // recieve the results
  recieve_from_slave();

  // send new problem to slave
  send_slave_data = true;
}

void send_to_slave(long min_val, long max_val) {
  String message = String(min_val) + ", " + String(max_val) + "\n";
  Serial.println("Sent: " + message);
  // enable Slave Select
  digitalWrite(SS, LOW);    // SS is pin 10
  
  // real string has to be maximum size of "<integer.MAX_VALUE>, <interger.MAX_VALUE>" == "-2147483648, -2147483648" => 24 chars

  for (unsigned int i = 0; i < message.length(); i++) {
    SPI.transfer(message[i]);
  }

  // disable Slave Select
  digitalWrite(SS, HIGH);

  current_slave_min = min_val;
  current_slave_max = max_val;

  slaveStartTime = millis();
}

void recieve_from_slave() {
  char buf [300];

  // enable Slave Select
  digitalWrite(SS, LOW);
  SPI.transfer (1);   // initiate transmission
  for (unsigned int pos = 0; pos < sizeof(buf) - 1; pos++) {
    delayMicroseconds (15);
    buf [pos] = SPI.transfer (0);
    if (buf [pos] == 0) {
      break;
    }
  }

  // ensure terminating null
  buf [sizeof (buf) - 1] = 0;

  // disable Slave Select
  digitalWrite(SS, HIGH);

  slave_answer = buf;
  recieved_slave_data = true;
}

// Serial.println(sizeof(3)); => 4
// Serial.println(sizeof(3L)); => 4
// Keep code always the same as in Slave
String primeTest(long min_num, long max_num) {
  long i, j, start_num, index_return = 0L;

  bool prime[(max_num - min_num) + 1];
  memset(prime, true, sizeof(prime));

  start_num = min_num;

  // here the computational work is done
  for (i = start_num; i <= max_num; i++) {
    for (j = 2; j <= i / 2; j++) {
      if ((i % j) == 0) {
        prime[index_return] = false;
        break;
      }
    }
    index_return++;
  }

  String ret_string = "";

  for (i = 0; i < (max_num - min_num) + 1; i++) {
    if (prime[i]) {
      // add long long compatibility
      String buff = String(min_num + i);
      ret_string += buff + ", ";
    }
  }

  if(ret_string.equals("")){
    return "NULL";
  }
  
  return ret_string.substring(0, ret_string.length() - 2);
}
